#!/bin/bash

set -eu -o pipefail
set -x

TAGDATE=$(date +%Y-%m-%d --utc)
DEFAULTREV=1
REV="${2-$DEFAULTREV}"
TAGDATE="${1-$TAGDATE}"
TAG="release/$TAGDATE/$REV"

echo
echo "Deleting $TAG in each submodule, if it exists"
git submodule foreach "git tag -d $TAG || true" > /dev/null 2>&1

echo
echo "Tagging submodules '$TAG'"
MSG="phabricator.wikimedia.org version $TAG"
TAGCMD="git tag -a $TAG -m \"$MSG\""
git submodule foreach "$TAGCMD"

echo
echo "Tagging parent repo"
git tag -d "$TAG" >/dev/null 2>&1 || true
git tag -a "$TAG" -m "$MSG"

git submodule foreach --quiet 'git describe'
git describe

echo
echo "Pushing deployment repo"
git push origin wmf/stable "$TAG"

echo
echo "Pushing submodules"
# Push tag and branch, set:
#   origin's wmf/stable === HEAD
#   origin's $TAG === local $TAG
git submodule foreach "git push origin HEAD:refs/heads/wmf/stable $TAG:$TAG"

# Current state of the scap plugin doesn't seem like it could work.
#
# echo "Creating phabricator milestone"
# scap tag milestone --project phabricator --template ./templates/phab.tmpl "$TAGDATE"
